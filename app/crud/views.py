from django_tables2 import SingleTableView

from .models import Containing, Dish, Product, Supplier, Delivery
from .tables import ContainingTable, DishTable, ProductTable, SupplierTable, DeliveryTable
from django.views.generic import TemplateView, CreateView, UpdateView
from django.shortcuts import get_object_or_404
from django.http import HttpRequest, HttpResponseRedirect, HttpResponse
from django.db import IntegrityError
import django_filters
from django_filters.views import FilterView
from django_filters import CharFilter
from django.views import View
from django.db import connection
from django.contrib.auth.mixins import LoginRequiredMixin


def supplier_delete(request: HttpRequest, pk: int):
    try:
        get_object_or_404(Supplier, pk=pk).delete()
    except IntegrityError:
        return HttpResponse('You must delete related objects', status=422)

    return HttpResponseRedirect('/supplier')


def dish_delete(request: HttpRequest, pk: int):
    try:
        get_object_or_404(Dish, pk=pk).delete()
    except IntegrityError:
        return HttpResponse('You must delete related objects', status=422)

    return HttpResponseRedirect('/dish')


def product_delete(request: HttpRequest, pk: int):
    try:
        get_object_or_404(Product, pk=pk).delete()
    except IntegrityError:
        return HttpResponse('You must delete related objects', status=422)

    return HttpResponseRedirect('/product')


def containing_delete(request: HttpRequest, pk: int):
    try:
        get_object_or_404(Containing, pk=pk).delete()
    except IntegrityError:
        return HttpResponse('You must delete related objects', status=422)

    return HttpResponseRedirect('/containing')


def delivery_delete(request: HttpRequest, pk: int):
    try:
        get_object_or_404(Delivery, pk=pk).delete()
    except IntegrityError:
        return HttpResponse('You must delete related objects', status=422)

    return HttpResponseRedirect('/delivery')


class IndexView(TemplateView):
    template_name = 'index.html'


class SupplierFilter(django_filters.FilterSet):
    name = CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Supplier
        fields = '__all__'


class SupplierIndexView(LoginRequiredMixin, CreateView, FilterView, SingleTableView):
    model = Supplier
    table_class = SupplierTable
    template_name = 'crud/supplier.html'
    fields = '__all__'
    success_url = '/supplier'
    filterset_class = SupplierFilter


class SupplierUpdateView(UpdateView):
    model = Supplier
    fields = '__all__'
    template_name = 'crud/update.html'
    success_url = '/supplier'
    extra_context = {'header': 'Supplier'}


class DishFilter(django_filters.FilterSet):
    name = CharFilter(field_name='name', lookup_expr='icontains')
    dish_type = CharFilter(field_name='dish_type', lookup_expr='icontains')

    class Meta:
        model = Dish
        fields = '__all__'
        exclude = ['number_of_servings']


class DishIndexView(LoginRequiredMixin, CreateView, FilterView, SingleTableView):
    model = Dish
    table_class = DishTable
    template_name = 'crud/dish.html'
    fields = '__all__'
    success_url = '/dish'
    filterset_class = DishFilter


class DishUpdateView(UpdateView):
    model = Dish
    fields = '__all__'
    template_name = 'crud/update.html'
    success_url = '/dish'
    extra_context = {'header': 'Dish'}


class ProcedureView(View):

    def get(self, request: HttpRequest, *args, **kwargs):
        cursor = connection.cursor()
        cursor.execute('''call delete_finished_dishes()''')
        return HttpResponseRedirect('/dish')


class ProductFilter(django_filters.FilterSet):
    name = CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Product
        fields = '__all__'
        exclude = ['calories_per_100_grams', 'quantity_in_grams']


class ProductIndexView(LoginRequiredMixin, CreateView, FilterView, SingleTableView):
    model = Product
    table_class = ProductTable
    template_name = 'crud/product.html'
    fields = '__all__'
    success_url = '/product'
    filterset_class = ProductFilter


class ProductUpdateView(UpdateView):
    model = Product
    fields = '__all__'
    template_name = 'crud/update.html'
    success_url = '/product'
    extra_context = {'header': 'Product'}


class ContainingIndexView(LoginRequiredMixin, CreateView, SingleTableView):
    model = Containing
    table_class = ContainingTable
    template_name = 'crud/containing.html'
    fields = '__all__'
    success_url = '/containing'


class ContainingUpdateView(UpdateView):
    model = Containing
    fields = '__all__'
    template_name = 'crud/update.html'
    success_url = '/containing'
    extra_context = {'header': 'Containing'}


class DeliveryIndexView(LoginRequiredMixin, CreateView, SingleTableView):
    model = Delivery
    table_class = DeliveryTable
    template_name = 'crud/delivery.html'
    fields = '__all__'
    success_url = '/delivery'


class DeliveryUpdateView(UpdateView):
    model = Delivery
    fields = '__all__'
    template_name = 'crud/update.html'
    success_url = '/delivery'
    extra_context = {'header': 'Delivery'}
