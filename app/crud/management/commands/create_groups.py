"""
Create permission groups
Create permissions (read only) to models for a set of groups
"""

from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group
from django.contrib.auth.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        manager = User(username='manager')
        manager.set_password('secret')
        manager.save()

        user = User(username='user')
        user.set_password('secret')
        user.save()

        manager_group, created = Group.objects.get_or_create(name='Managers')
        user_group, created = Group.objects.get_or_create(name='Users')

        manager_group.user_set.add(manager)
        user_group.user_set.add(user)
