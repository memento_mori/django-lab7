from django.urls import path

from crud.views import (ContainingIndexView, DishIndexView, ProductIndexView,
                        SupplierIndexView, DeliveryIndexView, IndexView, SupplierUpdateView, DishUpdateView,
                        ProductUpdateView, ContainingUpdateView, DeliveryUpdateView, ProcedureView)
from crud.views import supplier_delete, dish_delete, product_delete, containing_delete, delivery_delete

urlpatterns = [
    path('', DishIndexView.as_view(), name='dish_index'),
    path('supplier/', SupplierIndexView.as_view(), name='supplier'),
    path('supplier/delete/<int:pk>', supplier_delete, name='supplier_delete'),
    path('supplier/update/<int:pk>', SupplierUpdateView.as_view(), name='supplier_update'),

    path('dish/', DishIndexView.as_view(), name='dish'),
    path('dish/delete/<int:pk>', dish_delete, name='dish_delete'),
    path('dish/update/<int:pk>', DishUpdateView.as_view(), name='dish_update'),
    path('dish/procedure/', ProcedureView.as_view(), name='dish_procedure'),

    path('product/', ProductIndexView.as_view(), name='product'),
    path('product/delete/<int:pk>', product_delete, name='product_delete'),
    path('product/update/<int:pk>', ProductUpdateView.as_view(), name='product_update'),

    path('containing/', ContainingIndexView.as_view(), name='containing'),
    path('containing/delete/<int:pk>', containing_delete, name='containing_delete'),
    path('containing/update/<int:pk>', ContainingUpdateView.as_view(), name='containing_update'),

    path('delivery/', DeliveryIndexView.as_view(), name='delivery'),
    path('delivery/delete/<int:pk>', delivery_delete, name='delivery_delete'),
    path('delivery/update/<int:pk>', DeliveryUpdateView.as_view(), name='delivery_update'),
]
