from django.db import models


class Supplier(models.Model):
    name = models.CharField(max_length=50, null=False)

    def __str__(self):
        return self.name


class Dish(models.Model):
    name = models.CharField(max_length=50, null=False)
    dish_type = models.CharField(max_length=20, null=False)
    number_of_servings = models.PositiveIntegerField(null=False, default=10)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=50, null=False, unique=True)
    calories_per_100_grams = models.PositiveIntegerField(null=False)
    quantity_in_grams = models.PositiveIntegerField(null=False, default=10)

    def __str__(self):
        return self.name


class Containing(models.Model):
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    weight_in_grams = models.PositiveIntegerField(null=False, default=10)

    def __str__(self):
        return f'{self.dish.name} contains {self.product.name}'


class Delivery(models.Model):
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    cost = models.PositiveIntegerField(null=False, default=10)

    def __str__(self):
        return f'{self.supplier.name} delivers {self.product.name}'
