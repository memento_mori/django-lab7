import django_tables2 as tables

from .models import Containing, Dish, Product, Supplier, Delivery


class SupplierTable(tables.Table):
    actions = tables.TemplateColumn(
        template_name='buttons.html', extra_context={'entity': 'supplier'})

    class Meta:
        model = Supplier
        template_name = "django_tables2/bootstrap.html"
        fields = ("name",)


class DishTable(tables.Table):
    actions = tables.TemplateColumn(
        template_name='buttons.html', extra_context={'entity': 'dish'})

    class Meta:
        model = Dish
        template_name = "django_tables2/bootstrap.html"
        fields = ('name', 'dish_type', 'number_of_servings')


class ProductTable(tables.Table):
    actions = tables.TemplateColumn(
        template_name='buttons.html', extra_context={'entity': 'product'})

    class Meta:
        model = Product
        template_name = "django_tables2/bootstrap.html"
        fields = ('name', 'calories_per_100_grams', 'quantity_in_grams')


class ContainingTable(tables.Table):
    actions = tables.TemplateColumn(
        template_name='buttons.html', extra_context={'entity': 'containing'})

    class Meta:
        model = Containing
        template_name = "django_tables2/bootstrap.html"
        fields = ('dish', 'product', 'weight_in_grams')


class DeliveryTable(tables.Table):
    actions = tables.TemplateColumn(
        template_name='buttons.html', extra_context={'entity': 'delivery'})

    class Meta:
        model = Delivery
        template_name = "django_tables2/bootstrap.html"
        fields = ('supplier', 'product', 'cost')
