rm -rf app/crud/migrations/*
docker rm -f postgres
./run_db.sh
sleep 2
python3 app/manage.py makemigrations crud
python3 app/manage.py migrate
python3 app/manage.py loaddata init_db.json
PGPASSWORD=secret psql -h 127.0.0.1 -p 5432 -U postgres postgres -f procedure.sql
python3 app/manage.py create_groups
