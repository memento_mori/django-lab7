create procedure delete_finished_dishes()
    language sql
as
$$
with candidates as
         (
             select Id
             from crud_dish
             where number_of_servings = 0
         )
delete
from crud_containing
where dish_id in (select Id from candidates);

delete
from crud_dish
where number_of_servings = 0
$$